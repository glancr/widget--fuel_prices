require 'json'
$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'fuel_prices/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'fuel_prices'
  s.version     = FuelPrices::VERSION
  s.authors     = ['Tobias Grasse']
  s.email       = ['tg@glancr.de']
  s.homepage    = 'https://glancr.de/modules/fuel_prices'
  s.summary     = 'mirr.OS widget that displays current fuel prices.'
  s.description = 'See current fuel prices for gas stations around you.'
  s.license     = 'MIT'
  s.metadata    = { 'json' =>
                  {
                    type: 'widgets',
                    title: {
                      enGb: 'Fuel Prices',
                      deDe: 'Tanken',
                      frFr: 'Prix du carburant',
                      esEs: 'Precios del combustible',
                      plPl: 'Ceny paliwa',
                      koKr: '연료 가격'
                    },
                    description: {
                      enGb: s.description,
                      deDe: 'Lass\' dir aktuelle Kraftstoffpreise der Tankstellen in deiner Nähe anzeigen.',
                      frFr: 'Obtenez les prix actuels du carburant des stations-service les plus proches de chez vous.',
                      esEs: 'Obtenga los precios actuales de combustible de las gasolineras más cercanas.',
                      plPl: 'Uzyskaj aktualne ceny paliw na stacjach benzynowych w pobliżu.',
                      koKr: '가까운 주유소에서 현재 연료 가격을 확인하십시오.'
                    },
                    sizes: [
                      {
                        w: 5,
                        h: 4
                      }
                    ],
                    languages: %i[enGb deDe frFr esEs plPl koKr],
                    group: nil,
                    compatibility: '0.13.0'
                  }.to_json
                }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'rails', '~> 5.2'
end
